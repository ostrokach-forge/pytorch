#!/bin/bash

export AS="/opt/rh/devtoolset-2/root/usr/bin/as"
export AR="/opt/rh/devtoolset-2/root/usr/bin/ar"
export CC="/opt/rh/devtoolset-2/root/usr/bin/gcc"
export CXX="/opt/rh/devtoolset-2/root/usr/bin/g++"
export PATH="/opt/rh/devtoolset-2/root/usr/bin:${PATH}"

export CUDA_HOME='/usr/local/cuda-8.0'

OMP_NUM_THREADS=4 $PYTHON ./test/run_test.py
